import {useState, useEffect} from 'react';
import NavBar from './components/NavBar';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {Container} from 'react-bootstrap';
import Home from './pages/Home';
import Products from './pages/Product';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Admin from './pages/AdminProfile';
import CreateProduct from './components/CreateProduct';
import AdminProd from './pages/AdminProducts';
import Profile from './pages/Profile';
import Register from './pages/Register';
import ProductView from './components/ProductView';
import MyCart from './pages/MyCart';
/*import AllCart from './pages/allCart';*/
import Error from './pages/Error';
import './App.css';
//import Swal from 'sweetalert2'
import {UserProvider} from './UserContext';

function App(){

    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    })

    const unsetUser = () => {
        localStorage.clear();
    };

    useEffect(() => {
               fetch('http://localhost:4000/users/details', {                
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })

        .then(res => res.json())
        .then(data => {
            //captured the data of whoever is logged in
            console.log(data)

            if(typeof data._id !== 'undefined'){
                setUser({
                    is: data._id,
                    isAdmin: data.isAdmin
                });
            }else{
                setUser({
                    id: null,
                    isAdmin: null
                });
            };
        });
    }, [])


	return(
		<>
        <UserProvider value={{user, setUser, unsetUser}}>
		<Router>
                <NavBar/>
                <Container>
                    <Routes>
                        <Route exact path="/" element={<Home/>}/>
                        
                        <Route exact path="/products" element={<Products/>}/>
                        <Route exact path="/productsAdmin" element={<AdminProd/>}/>
                        <Route exact path="/createProd" element={<CreateProduct/>}/>
                        <Route exact path="/login" element={<Login/>}/>
                        <Route exact path="/Profile" element={<Profile/>}/>
                        <Route exact path="/productView/:prodId" element={<ProductView/>}/>
                        <Route exact path="/register" element={<Register/>}/>
                        <Route exact path="/admin" element={<Admin/>}/>
                        <Route exact path="/logout" element={<Logout/>}/>
                        <Route exact path="/myCart" element={<MyCart/>}/>
                        <Route exact path="*" element={<Error/>}/>
                    </Routes>
                </Container>
            </Router>
            </UserProvider>
		</>
		)
}
export default App