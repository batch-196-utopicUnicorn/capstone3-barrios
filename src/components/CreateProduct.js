import {useState} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import React from 'react'
import Swal from 'sweetalert2'


export default function CreateProduct(){

	const history = useNavigate()
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const addProduct =(e) => {
		e.preventDefault()
		fetch('https://immense-springs-08257.herokuapp.com/products/addProduct', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price, price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			Swal.fire({
					title: "Creating success",
					icon: "success",
					text: "You have successfully created a Product",
					background: '#fff url(../assets/background.jpg)'
				})
				history("/products")
		})

	}
		
		return(
			
			 <Form onSubmit={e => addProduct(e)}>
			 <>
			       <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Name</Form.Label>
         <Form.Select aria-label="Default select example" onChange={e => setName(e.target.value)}>
      		<option>Select Product Name</option>
      		<option value="Jet Ski">Jet Ski</option>
      		<option value="Banana Boat">Banana Boat</option>
      		<option value="UFO Boat">UFO Boat</option>
      		<option value="Paddleboarding">Paddleboarding</option>
     			<option value="Parasail">Parasail</option>
    		</Form.Select>
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                autoFocus
                 onChange={e => setPrice(e.target.value)}
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Description</Form.Label>
              <Form.Control as="textarea" rows={3} 
              onChange={e => setDescription(e.target.value)}/>
            </Form.Group>
             
      <Button variant="primary" type="submit">
        Submit
      </Button>
	</>
    </Form>

	)

}