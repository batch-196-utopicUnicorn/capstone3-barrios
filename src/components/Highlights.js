import {Row, Col, Card, Container} from 'react-bootstrap'
import Jetski from '../assets/jetski.jpg';
import BananaBoat from '../assets/bananaboat.jpg'
import Parasail from '../assets/parasail.jpg'
import {Link} from 'react-router-dom'


export default function Hightlights(){
	return(
		<Container className='center'>
		<Row className="m-3 text-center">
		<h1 className="my-4">Find Your Ride</h1>
			<Col xs={12} xl={4} className="my-4">
				<Card className="cardHighlight p-3">
				<Link className="links" as={Link} to="/productView/62eb2f81bc600743bfebebf4">
					<Card.Body>
						<Card.Title>
						<h2 className="mb-4">Jet Ski</h2>
						</Card.Title>
						 <img src={Jetski} className='img-fluid shadow-4' alt='...' />
						<Card.Text>
						Enjoy the thrill of speeding through the pristine waters of Boracay. There's no better way to explore Boracay than by an exhilarating Jet Ski Ride. Enjoy a 45-minute ride as you explore at your own pace.
						</Card.Text>
					</Card.Body>
					</Link>
				</Card>
			</Col>
			<Col xs={12} xl={4} className="my-4">
				<Card className="cardHighlight p-3">
				<Link className="links" as={Link} to="/productView/62eb2f81bc600743bfebebf4">
					<Card.Body>
						<Card.Title>
						<h2 className="mb-4">Banana Boat</h2>
						</Card.Title>
						<img src={BananaBoat} className='img-fluid shadow-4' alt='...' />
						<Card.Text>
						Have a safe family bonding in the blue waters of Boracay and get soaked by the splashing waves. Take in the offshore island views of Boracay and its famous white beach as you speed past them.
						</Card.Text>
					</Card.Body>
					</Link>
				</Card>
			</Col>
			<Col xs={12} xl={4} className="my-4">
			
				<Card className="cardHighlight p-3">
				<Link className="links" as={Link} to="/productView/62eb362b97b7ce35f9d1ff9f">
					<Card.Body>
						<Card.Title>
						<h2 className="mb-4">Parasail</h2>
						</Card.Title>
						<img src={Parasail} className='img-fluid shadow-4' alt='...' />
						<Card.Text>
						Enjoy a 15 minutes parasailing experience alone or with a partner. Observe the beauty of Boracay's coastline from high up and feel at ease with professional equipment and a life jacket provided for the activity.
						</Card.Text>
					</Card.Body>
					</Link>
				</Card>
				
			</Col>
		</Row>
		</Container>
	)
}