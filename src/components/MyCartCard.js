import { useState } from "react";

import ListGroup from 'react-bootstrap/ListGroup';

export default function OrderProducts({orderProp}){
    const{prodId, totalAmount} = orderProp
    const[productName, setProductName] = useState("")
    const [isLoading, setIsLoading] = useState(true)
    /*console.log(prodId)
    console.log(productName)
    console.log(totalAmount)
    console.log(quantity)*/

    fetch(`https://immense-springs-08257.herokuapp.com/products/singleProduct/${prodId}`
            ).then(res=>res.json())
            .then(data => {
                    setIsLoading(false)
                    setProductName(data.name)
            }).catch(err => console.log(err))

    return(
    	<>
    
      
      <ListGroup variant="flush">
        
        <ListGroup.Item>{productName}: ₱{totalAmount}</ListGroup.Item>
      </ListGroup>
    
         
         </>
    )

}
