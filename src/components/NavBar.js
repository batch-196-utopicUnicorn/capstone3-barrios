import {Navbar, Container, Nav} from 'react-bootstrap';
import Dropdown from 'react-bootstrap/Dropdown';
import {Link} from 'react-router-dom'
import {useContext} from 'react'
import UserContext from '../UserContext'

function ColorSchemesExample() {

  const {user} = useContext(UserContext)
  console.log(user.isAdmin)

  return (

      <Navbar className="navbar" bg="light" variant="light" expand='md' >
        <Container>
          <Navbar.Brand as={Link} to='/'>Tommy's WaterSport</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
          
          <Nav className="ms-auto">
            <Nav.Link className="mx-3" as={Link} to='/'>Home</Nav.Link>
            <Nav.Link className="mx-3" as={Link} to='/products'>Product</Nav.Link>
            {/*<Nav.Link className="mx-3" href="#features">Gallery</Nav.Link>*/}
            
            {
              (user.id !== null)?
            <>
            <Dropdown>
      <Dropdown.Toggle variant="light" id="dropdown-basic">
       Profile
      </Dropdown.Toggle>

      <Dropdown.Menu>
      {
        (user.isAdmin)?
        <>
        <Dropdown.Item as={Link} to='/profile'>My Profile</Dropdown.Item>
        <Dropdown.Item as={Link} to='/admin'>View Profiles</Dropdown.Item>
        <Dropdown.Item as={Link} to="/productsAdmin">View Products</Dropdown.Item>
        </>
        :
        <>
        <Dropdown.Item as={Link} to='/profile'>My Profile</Dropdown.Item>
        <Dropdown.Item as={Link} to='/myCart'>Cart list</Dropdown.Item>
        </>
      }
        <Dropdown.Item as={Link} to='/logout'>Logout</Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
            </>
            :
            <Nav.Link className="mx-3" as={Link} to='/login'>Login</Nav.Link>
          }
          </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

  );
}

export default ColorSchemesExample;

