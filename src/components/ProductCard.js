import {Card, Button, Container} from 'react-bootstrap';
import {useState, useContext} from 'react';
import {Link} from 'react-router-dom'
import UserContext from '../UserContext';

function ProductCard(props) {
  console.log(props);
  const {user} = useContext(UserContext)
  //console.log(typeof props)
  const  {name, description, price, _id, isActive} = props.productProp;

  console.log(isActive)

  return (
    <Container className="centerThis">
    <Card  className="p-3 mb-3 ">
      <Card.Body>
        <Card.Title className="mb-3">{name}</Card.Title>
        <Card.Text>
          <strong>Course description:</strong>
          <br/>
         {description}
        </Card.Text>
        <Card.Text>
          <strong>Course price:</strong>
          <br/>
         ₱{price}
        </Card.Text>
        {
          (user.isAdmin == true)?
          (isActive !== false)?
          <h2>Active</h2>
          :
          <h2>Inactive</h2>
          :
          <h2></h2>
        }
        <Link className="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>
      </Card.Body>
    </Card>
    </Container>
  );
}

export default ProductCard;