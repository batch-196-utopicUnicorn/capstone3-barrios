import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import UserContext from '../UserContext';
import {useContext, useEffect, useState} from 'react';
import {useParams, useNavigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2';
import React from 'react'
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import {Container} from 'react-bootstrap';

export default function ProductView(){

	const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

	const {user} = useContext(UserContext)
	console.log(user.isAdmin)
	const history = useNavigate()

	const {prodId} = useParams()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState();

const [newName, setNewName] = useState('');
const [newDescription, setNewDescription] = useState('');
const [newPrice, setNewPrice] = useState(0)

	let picture = ''

	if(name === "Jet Ski"){
		 picture = require("../assets/jetski.jpg")
	}else if(name === "Banana Boat"){
		 picture = require("../assets/bananaboat.jpg")
	}else if(name === "Parasail"){
		 picture = require("../assets/parasail.jpg")
	}else if(name === "UFO Boat"){
		picture = require('../assets/ufo.jpg')
	}else if(name === "Paddleboarding"){
		picture = require('../assets/Paddleboarding.jpg')
	}

	const token = localStorage.getItem('token')

	const purchase = (prodId) => {

		fetch('https://immense-springs-08257.herokuapp.com/users/createOrder', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				prodId: prodId,
				totalAmount: price,
				quantity: 1  

			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			console.log(prodId)
			if(data){
				Swal.fire({
					title: 'Purchase Success',
					icon: "success",
					text: "Thank you for purchasing"
				});

				history('/products')
			} 
			else{
				Swal.fire({
					title: 'Something went wrong',
					icon: "error",
					text: "Please try again"
				})
			}
		})
	};

	const unDeleteProduct = () => {
		console.log('Being clicked here')
		fetch(`https://immense-springs-08257.herokuapp.com/products/unArchive/${prodId}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${token}`
			}})
		.then(res => res.json())
		Swal.fire({
					title: 'Undelete Success',
					icon: "success",
					text: "You have successfully undeleted this product."
				}).then((result) => {
					refreshPage()
				})
	}

	const deleteProduct = () => {
		console.log('Being clicked')
		fetch(`https://immense-springs-08257.herokuapp.com/products/archive/${prodId}`, {
			method: 'DELETE',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${token}`
			}})
		.then(res => res.json())
		if(isActive !== false){
			
			Swal.fire({
					title: 'Delete Success',
					icon: "success",
					text: "You have successfully deleted this product."
				}).then((result) => {
					refreshPage()
				})

		}
	}


	const updateProduct = (e) => {
		

		fetch(`https://immense-springs-08257.herokuapp.com/products/updateProduct/${prodId}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
						name: newName,
						description: newDescription,
						price: newPrice
		})
		
	})
	.then(res => res.json())
	.then(data => 
	console.log(data))
	}



	useEffect(() => {
		console.log(prodId)
		fetch(`https://immense-springs-08257.herokuapp.com/products/singleProduct/${prodId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data.isActive)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setIsActive(data.isActive)
		})
	}, [prodId])

	function refreshPage() {
    window.location.reload(false);
  }

  function succeed(){
  	Swal.fire({
					title: "Update Sucessful",
					icon: "success",
					text: "You have successfully updated the product",
					background: '#fff url(../assets/background.jpg)'
				})
  }

	return(
		<>
		<Container>
		<Card className="createHight">
      <Card.Img variant="top" src={picture} />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description</Card.Subtitle>
        <Card.Text>
          {description}
        </Card.Text>
        <ListGroup className="flush">
        <ListGroup.Item>₱{price}</ListGroup.Item>

        {
        	(isActive === false)?
        		<ListGroup.Item>This is not available</ListGroup.Item>
        		:
        		<ListGroup.Item>This is available</ListGroup.Item>
        }

        
      </ListGroup>



        {
        (user.id !== null)?   
        	(user.isAdmin !== false)?
        <>
        		<Button variant="primary" onClick={handleShow}>Edit</Button>
        		{
        		(isActive !== false)?
        		<Button variant="danger" onClick={() => deleteProduct()}>Deactivate</Button>
        		:
        		<Button variant="primary" onClick={() => unDeleteProduct()}>Activate</Button>
        	}
        		<Button variant="primary" as={Link} to="/productsAdmin">Back</Button>
        </>
			:
        <>
        		<Button variant="primary" onClick={() => purchase(prodId)}>Purchase</Button>
        		<Button variant="primary" as={Link} to="/products">Back</Button>
        </>
        	:
        		<Button variant="primary" as={Link} to="/products">Back</Button>
        
        }

			
         <Modal show={show} onHide={handleClose} >
        <Modal.Header closeButton>
          <Modal.Title>Update Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Name</Form.Label>
               <Form.Select aria-label="Default select example" onChange={e => setNewName(e.target.value)}>
      					<option>Select Product Name</option>
      					<option value="Jet Ski">Jet Ski</option>
      					<option value="Banana Boat">Banana Boat</option>
     					 <option value="Parasail">Parasail</option>
     					 <option value="UFO Boat">UFO Boat</option>
      					<option value="Paddleboarding">Paddleboarding</option>
    					</Form.Select>
              {/*<Form.Control
                type="text"
                autoFocus
                onChange={e => setNewName(e.target.value)}
              />*/}
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="email"
                autoFocus
                 onChange={e => setNewPrice(e.target.value)}
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Description</Form.Label>
              <Form.Control as="textarea" rows={3} 
              onChange={e => setNewDescription(e.target.value)}/>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose} onClick={refreshPage}>
            Close
          </Button>
          <Button variant="primary" type="submit" id="submitBtn" onClick={e => updateProduct(e)} onClick={succeed}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
      

      </Card.Body>
    </Card>
    </Container>
     
    </>
	)
}