import {Card, Button, Container} from 'react-bootstrap';
import {useState} from 'react';
import {Link} from 'react-router-dom'


function UserCard(props) {
  //console.log(props);
  //console.log(typeof props)
  const  {firstName, lastName, mobileNo, email, _id} = props.userProp;
  //console.log(firstName)


  return (
    <>
    <Container>

    <Card>
        
      <Card.Body>
      <Card.Title className="mb-3">Full Name: {firstName} {lastName}</Card.Title>
        <Card.Text className="mb-1"><strong>Mobile number:</strong> {mobileNo}</Card.Text>
        <Card.Text className="mb-1"><strong>Email address:</strong> {email}</Card.Text>
        {/*<Button variant="primary">View User</Button>*/}
      </Card.Body>
    </Card>
    
    </Container>
    </>
  );
}

export default UserCard;