import {useState, useEffect, useContext} from 'react';
import ProductCard from '../components/ProductCard'
import UserContext from '../UserContext';
import {Link} from 'react-router-dom'


export default function AdminProducts(){

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext)
	console.log(user.isAdmin)
/*
	(user.isAdmin !== true)*/

		useEffect(() => {
			
		fetch('https://immense-springs-08257.herokuapp.com/products/getAllProduct')
		.then(res => res.json())
		.then(data => {

			console.log(data)

			setProducts(data.map(prod =>{
				return(
					<ProductCard key={prod._id} productProp ={prod}/>
					)
			}))
		})
	}, [])

	return(
		<>
		<h1>Available Rides:</h1>
		{
			(user.isAdmin !== false)?
			<Link className="btn btn-primary" to={`/createProd`}>Create Product</Link>
			:
			<button></button>
		}
		{products}
		</>
	)

}