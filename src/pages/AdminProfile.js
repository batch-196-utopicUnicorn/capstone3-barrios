import UserContext from '../UserContext'
import {useContext} from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import {useState, useEffect} from 'react';
import UserCard from '../components/UserCard'



export default function AdminProfile(){
	const {user} = useContext(UserContext)
  	//console.log(UserCard)


  	const [users, setUsers] = useState([]);

	useEffect(() => {
		fetch('https://immense-springs-08257.herokuapp.com/users/getAllUsers',{
			headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			setUsers(data.map(user =>{
				return(
					<UserCard key={user._id} userProp ={user}/>
					)
			}))
		})
	}, [])



	return(
		<>
		<h1>List of Users</h1>
		{users}
		</>
	)
}