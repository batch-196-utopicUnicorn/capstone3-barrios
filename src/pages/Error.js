import {Card, Container} from 'react-bootstrap';
import dog from '../assets/dog.jpg'

export default function Error(){
	return(
		<Container className="form-cont">
		 <Card className="m-auto text-center">
        <Card.Img variant="top" className="img-fluid dogImg" src={dog} />
        <Card.Body>
        <Card.Subtitle>
        SORRY
        </Card.Subtitle>
          <Card.Text>
            We couldn't find that page
          </Card.Text>
        </Card.Body>
      </Card>
      </Container>
	)
}