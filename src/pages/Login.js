import {useContext, useState, useEffect} from 'react';
import {Navigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Swal from 'sweetalert2'

export default function Login(){

	const {user, setUser} = useContext(UserContext)


	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isLoggedIn, setLoggedIn] = useState(false)

	function loginUser(e){
		e.preventDefault();

		fetch('https://immense-springs-08257.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email:email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.accessToken !== 'undefined'){
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Sucessful",
					icon: "success",
					text: "Welcome back.",
					background: '#fff url(../assets/background.jpg)'
				})
			}else{

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your credentials",
					footer: '<a as={Link} to="/register">You may need to register</a>'
				})
			}
		})
	}
	const retrieveUserDetails = (token) => {

		fetch('https://immense-springs-08257.herokuapp.com/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		})
	};

	useEffect(() =>{
		//validation to enable the submit button when all fields are populated and both passwords match
		if(email !== '' && password !== ''){
			setLoggedIn(true)
		} else{
			setLoggedIn(false)
		}

	}, [email, password])



	return(
		(user.id !== null) ?
			<Navigate to ='/'/>
		:
		<>
		<div className='form-cont'>
		<Form onSubmit={e => loginUser(e)} className="loginForm">
		<h1 className='text-center'>Login</h1>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" value = {email} onChange = {e => setEmail(e.target.value)}/>
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value = {password} onChange = {e => setPassword(e.target.value)}/>
      </Form.Group>
      {/*<Form.Group className="mb-3" controlId="formBasicCheckbox">
        <Form.Check type="checkbox" label="Check me out" />
      </Form.Group>*/}
      <p>Not yet registered? <Link to="/register"> Register here</Link></p>


      { isLoggedIn ?
      <Button variant="primary" type="submit">
        Login
      </Button>
      :

      <Button variant="danger" type="submit" disabled>
        Login
      </Button>

      }
    </Form>
    </div>
    </>
	)
}