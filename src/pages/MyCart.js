import UserContext from '../UserContext'
import {useContext} from 'react'
import {useState, useEffect} from 'react';
import MyCartCard from '../components/MyCartCard'
import Card from 'react-bootstrap/Card';
import {Container} from 'react-bootstrap';

export default function MyCart(){

	const {user} = useContext(UserContext)
  	console.log(user)
  	const [orders , setOrders] = useState([]);
  	const [TotalAmount , setTotalAmount] = useState([]);
  	console.log(TotalAmount)

  	/*let result2 = orders.map(a => a.prodId);
  	console.log(result2)*/



  	let result = TotalAmount.map(a => a.totalAmount);
  	console.log(result)
  	const realTotalAmount = result.reduce((partialSum, a) => partialSum + a, 0)
  	console.log(realTotalAmount)
  	

  	/*users.forEach(price => {
  		for(let key in price){
  			console.log(`${price[key]}`)
  		}
  	})*/

  	/*const [name, setName] =
*/
  	/*const RetrieveSingleProduct = () => {

  		 const [name, setName] = useState('');
  		console.log(name)
  		
  		useEffect(() => {
  			for(let i = 0; i < result2.length; i++ ){
  			fetch(`http://localhost:4000/products/singleProduct/${result2[i]}`,)
  			.then(res => res.json())
  			.then(data => setName(data))
  			}
  		}, '')



  	}*/

  	/*RetrieveSingleProduct()*/
  	/*if(user !== isAdmin){

  	}*/
  	useEffect(() => {
		fetch('https://immense-springs-08257.herokuapp.com/users/getUserOrders',{
			headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
		})
		.then(res => res.json())
		.then(data => {
			setTotalAmount(data)
			setOrders(data.map(order =>{
				return(
					<MyCartCard key={order._id} orderProp ={order}/>
					)})
			
		)
	})}, [])


	return(
		<>
		<div className='form-cont'>
		<Container className="cartForm ">
		<Card className='centerMe' style={{ width: '18rem' }}>
		<h1 className="text-center">My Cart</h1>
		<br/>
		<Card.Text>Item List:</Card.Text>
		{orders}
		<Card.Body>
		<Card.Text>Quantity: {TotalAmount.length}</Card.Text>
		<Card.Text>Total Amount: ₱{realTotalAmount}</Card.Text>
		</Card.Body>
		</Card>
		</Container>
		</div>
		</>
	)

}