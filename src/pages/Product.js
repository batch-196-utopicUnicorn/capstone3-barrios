import {useState, useEffect, useContext} from 'react';
import ProductCard from '../components/ProductCard'
import UserContext from '../UserContext';
import {Link} from 'react-router-dom'


export default function Products(){

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext)
	console.log(user.isAdmin)
/*
	(user.isAdmin !== true)*/

		useEffect(() => {
			
		fetch('https://immense-springs-08257.herokuapp.com/products/activeProduct')
		.then(res => res.json())
		.then(data => {

			console.log(data)

			setProducts(data.map(prod =>{
				return(
					<ProductCard key={prod._id} productProp ={prod}/>
					)
			}))
		})
	}, [])
	

	

	

	//function addProduct()

	return(
		<>
		<h1>Available Rides:</h1>
		
		{products}
		</>
		)
}

/*else{
				fedtch = fetch('http://localhost:4000/products/getAllProduct')
			}*/