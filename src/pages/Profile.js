import UserContext from '../UserContext'
import {useContext} from 'react'
import {useState, useEffect} from 'react';
import ProfileCard from '../components/ProfileCard'
import Form from 'react-bootstrap/Form';
import {Link} from 'react-router-dom'
import Button from 'react-bootstrap/Button';
import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import Stack from 'react-bootstrap/Stack';



export default function Profile(){
const {user} = useContext(UserContext)
  	console.log(user)

  	const [users, setUsers] = useState([]);
const fullname = `${users.firstName} ${users.lastName}`

	useEffect(() => {
		fetch('https://immense-springs-08257.herokuapp.com/users/details',{
			headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
		})
		.then(res => res.json())
		.then(data => { setUsers(data)
			/*setUsers(data.map(user =>{
				return(
					<ProfileCard key={user._id} profileProp ={user}/>
					)
			}))*/
			
		})
	}, [])
	console.log(users)

	return(
	

      <>
		<div className='form-cont'>
		<Form  className="loginForm">
		<h1 className='text-center'>{users.firstName}'s Profile</h1>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Full Name:</Form.Label>
        <Form.Control type="text"  value = {fullname}  disabled/>
        
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Email:</Form.Label>
        <Form.Control type="text"  value = {users.email} disabled/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Mobile number:</Form.Label>
        <Form.Control type="text"  value = {users.mobileNo} disabled/>
      </Form.Group>

      {
      (user.isAdmin !== false)?
      <h2></h2>
      :
      <Stack gap={2} className="col-md-5 mx-auto">
      <Link className="btn btn-primary"  as={Link} to='/myCart'>View Cart</Link>
      </Stack>
    	}
     
     
    </Form>
    </div>
    
    </>
    
	)
}